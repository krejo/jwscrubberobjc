//
//  MasterViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/17/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "JWScrubber.h"

@interface MasterViewController () <JWDetailDelegate>
@property NSMutableArray *objects;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self documentsDirectoryPath];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    //    self.clearsSelectionOnViewWillAppear = NO;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    [self readUserOrderedList];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(NSMutableDictionary*)scrubberConfigNew {
    
    NSMutableDictionary *result = nil;
    
    NSString *cacheKey = [[NSUUID UUID] UUIDString];
    
    NSMutableDictionary * scrubberOptions =
    [@{@"size":@(SampleSize14),
       @"kind":@(VABOptionCenter),
       @"layout":@(VABLayoutOptionOverlayAverages | VABLayoutOptionShowAverageSamples | VABLayoutOptionShowCenterLine)
       } mutableCopy];
    
    NSMutableDictionary * scrubberFileReference =
    [@{@"duration":@(0),
       @"startinset":@(0.0),
       @"endinset":@(0.0),
       } mutableCopy];
    
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"trimmedMP3_0CB28386-7EE7-4C7B-9E86-BDCBA0D15FB1" withExtension:@".m4a"];
    
    // The object to INSERT
    
    result =
    [@{@"key":cacheKey,
       @"options":scrubberOptions,
       @"title":@"scrubber",
       @"tracks":@(1),
       @"starttime":@(0.0),
       @"referencefile": scrubberFileReference,
       @"date":[NSDate date],
       @"fileURL":fileURL
       } mutableCopy];
    
    //    @"scrubbercolors":colors
    //    @"trackcolors":colors
    
    return result;
}


- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    
    NSMutableDictionary *scrubberObject = [self scrubberConfigNew];

    [self.objects insertObject:scrubberObject atIndex:0];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSMutableDictionary *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
        controller.delegate = self;
    }
}


#pragma mark - helpers

-(NSUInteger)indexOfCacheItem:(NSString*)key
{
    NSUInteger index = 0;
    for (id item in _objects) {
        if ([key isEqualToString:item[@"key"]]) {
            // found it
            break;
        }
        index++;
    }

    NSLog(@"%s%@ index %ld",__func__,key,index);
    return index;
}

-(void)reloadItemAtIndex:(NSUInteger)index {
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]
                          withRowAnimation:UITableViewRowAnimationFade];
    
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                animated:YES scrollPosition:UITableViewScrollPositionNone];

}

#pragma mark - delegate methods

-(void)itemChanged:(DetailViewController*)controller {
    [self saveUserOrderedList];
}

-(void)save:(DetailViewController*)controller cachKey:(NSString*)key
{
    NSLog(@"%s%@",__func__,key);
    NSUInteger index = [self indexOfCacheItem:key];
    [self reloadItemAtIndex:index];
    
    [self saveUserOrderedList];
}

-(void)itemChanged:(DetailViewController*)controller cachKey:(NSString*)key {
    NSLog(@"%s%@",__func__,key);
    NSUInteger index = [self indexOfCacheItem:key];
    [self reloadItemAtIndex:index];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDictionary *object = self.objects[indexPath.row];
    
    NSMutableDictionary * scrubberOptions  = object[@"options"];
    NSDictionary * scrubberFileReference = object[@"referencefile"];
    NSUInteger tracks =  [object[@"tracks"] unsignedIntegerValue];
    
    float startInset = [scrubberFileReference[@"startinset"] floatValue];
    float endInset = [scrubberFileReference[@"endinset"] floatValue];
//    float duration = [scrubberFileReference[@"duration"] floatValue];
//    NSLog(@"%s %.2f %.2f %.2f tracks %ld",__func__,startInset,endInset,duration,tracks);
    id startTimeValue = object[@"starttime"];
    float startTime = startTimeValue ? [startTimeValue floatValue] : 0.0;

//    NSString *refFileStr = [NSString stringWithFormat:@"%0.3f [%.2f, %.2f]",startTime,startInset,endInset];
    NSString *refFileStr = [NSString stringWithFormat:@"[%.2f, %.2f]",startInset,endInset];

//    NSURL *fileURL = object[@"fileURL"];
    
    VABLayoutOptions layout =  [[scrubberOptions valueForKey:@"layout"] unsignedIntegerValue];
    SampleSize ssz =  [[scrubberOptions valueForKey:@"size"] unsignedIntegerValue];
    VABKindOptions kind =  [[scrubberOptions valueForKey:@"kind"] unsignedIntegerValue];

    NSMutableString *optionsString = [NSMutableString new];
    if (layout & VABLayoutOptionShowAverageSamples)
        [optionsString appendString:@"Averages "];
    if (layout & VABLayoutOptionShowHashMarks)
        [optionsString appendString:@"Hash "];
    if (layout & VABLayoutOptionStackAverages)
        [optionsString appendString:@"Stack "];
    if (layout & VABLayoutOptionOverlayAverages)
        [optionsString appendString:@"Overlay "];
    if (layout & VABLayoutOptionShowCenterLine)
        [optionsString appendString:@"CenterLine "];
    
    NSString *samplSizeStr;
    if (ssz == SampleSize4){
        samplSizeStr = @"SampleSize4";
    } else if (ssz == SampleSize8) {
        samplSizeStr = @"SampleSize8";
    } else if (ssz == SampleSize10) {
        samplSizeStr = @"SampleSize10";
    } else if (ssz == SampleSize14) {
        samplSizeStr = @"SampleSize14";
    } else if (ssz == SampleSize18) {
        samplSizeStr = @"SampleSize18";
    }
    
    NSString *kindStr;
    if (kind == VABOptionCenter) {
        kindStr = @"Center";
    } else if (kind == VABOptionSingleChannelTopDown) {
        kindStr = @"TOP DOWN";
    } else if (kind == VABOptionSingleChannelBottomUp) {
        kindStr = @"BOTTOM UP";
    }

    cell.textLabel.text = [NSString stringWithFormat:@"%@%@ - %ldtr start %.2fs",object[@"title"],samplSizeStr,tracks,startTime] ;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ | %@ | %@",optionsString,kindStr,refFileStr];

    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self saveUserOrderedList];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

#pragma mark -

-(NSString*)documentsDirectoryPath {
    NSString *result = nil;
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    result = [searchPaths objectAtIndex:0];
    
    NSLog(@"%s %@",__func__, result);
    return result;
}

//-(NSString*)documentsDirectoryPath {
//    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    return [searchPaths objectAtIndex:0];
//}
-(NSURL *)fileURLForCacheItem:(NSString*)name {
    NSURL *result;
    NSString *thisfName = name;//@"mp3file";
    NSString *thisName = thisfName; //[NSString stringWithFormat:@"%@_%@.mp3",thisfName,dbkey?dbkey:@""];
    NSMutableString *fname = [[self documentsDirectoryPath] mutableCopy];
    [fname appendFormat:@"/%@",thisName];
    //    NSLog(@"%s %@",__func__,fname);  // directory
    result = [NSURL fileURLWithPath:fname];
    //    NSLog(@"%s FileName: %@",__func__,[result lastPathComponent]);
    return result;
}

/*
 serializeOut
 Convert any dictionary items that cannot be serialized to serializable format if possible
 fileURL NSURL to Path
 UIColor to rgb alpha
 */
-(void)serializeOut {
    for (id obj in _objects) {
        
        id furl = obj[@"fileURL"];
        if (furl) {
            obj[@"fileURLStr"] = [(NSURL*)furl path];
            [obj removeObjectForKey:@"fileURL"];
        }
        
        id scrubberColors = obj[@"scrubbercolors"];
        if (scrubberColors) {
            __block NSMutableDictionary *writableScrubberColors = [NSMutableDictionary new];
            [(NSDictionary*)scrubberColors enumerateKeysAndObjectsUsingBlock: ^(id key,id colorObj,BOOL *stop){
                
                CGFloat rd;
                CGFloat gr;
                CGFloat bl;
                CGFloat alpha;
                
                [(UIColor*)colorObj getRed:&rd green:&gr blue:&bl alpha:&alpha];
                
                writableScrubberColors[key] = @{@"red":@(rd),
                                                @"green":@(gr),
                                                @"blue":@(bl),
                                                @"alpha":@(alpha)
                                                };
            }];
            
            obj[@"scrubbercolors"] = writableScrubberColors;
        }
        
        
        id trackColors = obj[@"trackcolors"];
        if (trackColors) {
            __block NSMutableDictionary *writableScrubberColors = [NSMutableDictionary new];
            [(NSDictionary*)trackColors enumerateKeysAndObjectsUsingBlock: ^(id key,id colorObj,BOOL *stop){
                
                CGFloat rd;
                CGFloat gr;
                CGFloat bl;
                CGFloat alpha;
                
                [(UIColor*)colorObj getRed:&rd green:&gr blue:&bl alpha:&alpha];
                
                writableScrubberColors[key] = @{@"red":@(rd),
                                                @"green":@(gr),
                                                @"blue":@(bl),
                                                @"alpha":@(alpha)
                                                };
            }];
            
            obj[@"trackcolors"] = writableScrubberColors;
        }
    }
}



-(void)saveUserOrderedList {
    [self serializeOut];
    [_objects writeToURL:[self fileURLForCacheItem:@"savedobjects"]atomically:YES];
    [self serializeIn];

    NSLog(@"%s savedobjects[%ld]",__func__,[_objects count]);
//    NSLog(@"%savedobjects \n%@",__func__,[_objects description]);
    
}

/*
 serializeIn
 Convert any dictionary items that could not be serialized and were converted to a serializable format
 Path to fileURL NSURL
 rgb-alpha to UIColor
 */

-(void)serializeIn {
    
    for (id obj in _objects) {
        id furl = obj[@"fileURLStr"];
        if (furl) {
            obj[@"fileURL"] = [NSURL fileURLWithPath:(NSString*)furl];
            [obj removeObjectForKey:@"fileURLStr"];
        }
        
        id scrubberColors = obj[@"scrubbercolors"];
        if (scrubberColors) {
            NSLog(@"%s scrubberColors \n%@",__func__,[scrubberColors description]);
            __block NSMutableDictionary *readScrubberColors = [NSMutableDictionary new];
            
            [(NSDictionary*)scrubberColors enumerateKeysAndObjectsUsingBlock: ^(id key,id colorObj,BOOL *stop){
                readScrubberColors[key] =
                [[UIColor alloc] initWithRed:[colorObj[@"red"] floatValue]
                                       green:[colorObj[@"green"] floatValue]
                                        blue:[colorObj[@"blue"] floatValue]
                                       alpha:[colorObj[@"alpha"] floatValue]];
            }];
            
            obj[@"scrubbercolors"] = readScrubberColors;
        }
        
        id trackColors = obj[@"trackcolors"];
        if (trackColors) {
            NSLog(@"%s trackColors \n%@",__func__,[trackColors description]);
            __block NSMutableDictionary *readScrubberColors = [NSMutableDictionary new];
            
            [(NSDictionary*)trackColors enumerateKeysAndObjectsUsingBlock: ^(id key,id colorObj,BOOL *stop){
                readScrubberColors[key] =
                [[UIColor alloc] initWithRed:[colorObj[@"red"] floatValue]
                                       green:[colorObj[@"green"] floatValue]
                                        blue:[colorObj[@"blue"] floatValue]
                                       alpha:[colorObj[@"alpha"] floatValue]];
            }];
            
            obj[@"trackcolors"] = readScrubberColors;
        }
    }
    
}

-(void)readUserOrderedList {
    
    _objects = [[NSMutableArray alloc] initWithContentsOfURL:[self fileURLForCacheItem:@"savedobjects"]];

    [self serializeIn];
  
    NSLog(@"%savedobjects[%ld]",__func__,[_objects count]);
//    NSLog(@"%savedobjects \n%@",__func__,[_objects description]);
    
}

@end
