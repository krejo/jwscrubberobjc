//
//  DetailViewController.h
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/17/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JWDetailDelegate;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak,nonatomic) id <JWDetailDelegate> delegate;
@end


@protocol JWDetailDelegate <NSObject>
-(void)itemChanged:(DetailViewController*)controller;
-(void)itemChanged:(DetailViewController*)controller cachKey:(NSString*)key;
-(void)save:(DetailViewController*)controller cachKey:(NSString*)key;
@end

