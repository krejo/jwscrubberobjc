//
//  JWScrubberColorTableViewController.h
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/24/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JWScrubberColorDelegate;

@interface JWScrubberColorTableViewController : UITableViewController
@property (weak,nonatomic) id <JWScrubberColorDelegate> delegate;
@property (nonatomic) UIColor *selectedColor;
@end


@protocol JWScrubberColorDelegate <NSObject>
-(void)colorSelected:(JWScrubberColorTableViewController*)controller;
@end
