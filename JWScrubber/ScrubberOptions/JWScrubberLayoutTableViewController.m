//
//  JWScrubberLayoutTableViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "JWScrubberLayoutTableViewController.h"
#import "JWScrubberColorTableViewController.h"

@interface JWScrubberLayoutTableViewController () <JWScrubberColorDelegate>
@property (nonatomic) NSString *colorSpec;
@property (nonatomic) NSMutableDictionary *trackColorsConfig;
@end

@implementation JWScrubberLayoutTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController]) {
        NSLog(@"%s LEAVING",__func__);
        [_delegate willComplete:self];
    }
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (NSDictionary*)trackColors {
    return [NSDictionary dictionaryWithDictionary:_trackColorsConfig];
}

-(void)setTrackColors:(NSDictionary *)trackColors {
    if (trackColors) {
        _trackColorsConfig = [trackColors mutableCopy];
    } else {
        _trackColorsConfig = [@{} mutableCopy];
    }
}

-(void)colorSelected:(JWScrubberColorTableViewController*)controller {
    _trackColorsConfig[_colorSpec] = controller.selectedColor;
    _colorSpec = nil;
}


#pragma mark - Table view data source

//    VABLayoutOptionNone
//  0  VABLayoutOptionShowAverageSamples
//  1  VABLayoutOptionShowHashMarks
//  2  VABLayoutOptionStackAverages
//  3  VABLayoutOptionOverlayAverages
//  4  VABLayoutOptionShowCenterLine

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
 
    // LAYOUT
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            if (_layoutOptions & VABLayoutOptionShowAverageSamples) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } else if (indexPath.row == 1) {
            if (_layoutOptions & VABLayoutOptionShowHashMarks) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } else if (indexPath.row == 2) {
            if (_layoutOptions & VABLayoutOptionStackAverages) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } else if (indexPath.row == 3) {
            if (_layoutOptions & VABLayoutOptionOverlayAverages) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } else if (indexPath.row == 4) {
            if (_layoutOptions & VABLayoutOptionShowCenterLine) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
    }
    
    // COLORS
    
    else if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            id color = _trackColorsConfig[JWColorScrubberTopPeak];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 1) {
            id color = _trackColorsConfig[JWColorScrubberTopAvg];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 2) {
            id color = _trackColorsConfig[JWColorScrubberBottomPeak];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 3) {
            id color = _trackColorsConfig[JWColorScrubberBottomAvg];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 4) {
            id color = _trackColorsConfig[JWColorScrubberTopPeakNoAvg];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 5) {
            id color = _trackColorsConfig[JWColorScrubberBottomPeakNoAvg];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    // LAYOUT
    
    if (indexPath.section == 0) {
        
        _colorSpec = nil;

        if (indexPath.row == 0) {
            if (_layoutOptions & VABLayoutOptionShowAverageSamples) {
                _layoutOptions &= ~VABLayoutOptionShowAverageSamples;
            } else {
                _layoutOptions |= VABLayoutOptionShowAverageSamples;
            }
        } else if (indexPath.row == 1) {
            if (_layoutOptions & VABLayoutOptionShowHashMarks) {
                _layoutOptions &= ~VABLayoutOptionShowHashMarks;
            } else {
                _layoutOptions |= VABLayoutOptionShowHashMarks;
            }
        } else if (indexPath.row == 2) {
            if (_layoutOptions & VABLayoutOptionStackAverages) {
                _layoutOptions &= ~VABLayoutOptionStackAverages;
            } else {
                _layoutOptions |= VABLayoutOptionStackAverages;
                // update Overlay
                _layoutOptions &= ~VABLayoutOptionOverlayAverages;
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
        } else if (indexPath.row == 3) {
            if (_layoutOptions & VABLayoutOptionOverlayAverages) {
                _layoutOptions &= ~VABLayoutOptionOverlayAverages;
            } else {
                _layoutOptions |= VABLayoutOptionOverlayAverages;
                // update Stack
                _layoutOptions &= ~VABLayoutOptionStackAverages;
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
        } else if (indexPath.row == 4) {
            if (_layoutOptions & VABLayoutOptionShowCenterLine) {
                _layoutOptions &= ~VABLayoutOptionShowCenterLine;
            } else {
                _layoutOptions |= VABLayoutOptionShowCenterLine;
            }
        }

        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [_delegate selectionLayoutMade:self];

    }
    
    // COLORS
    
    else if (indexPath.section == 1) {
       
        if (indexPath.row == 0) {
            _colorSpec = (NSString*)JWColorScrubberTopPeak;
        } else if (indexPath.row == 1) {
            _colorSpec = (NSString*)JWColorScrubberTopAvg;
        } else if (indexPath.row == 2) {
            _colorSpec = (NSString*)JWColorScrubberBottomPeak;
        } else if (indexPath.row == 3) {
            _colorSpec = (NSString*)JWColorScrubberBottomAvg;
        } else if (indexPath.row == 4) {
            _colorSpec = (NSString*)JWColorScrubberTopPeakNoAvg;
        } else if (indexPath.row == 5) {
            _colorSpec = (NSString*)JWColorScrubberBottomPeakNoAvg;
        }
        
        [self performSegueWithIdentifier:@"JWScrubberLayoutToColorSegue" sender:self];
    }
    
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    // Configure the cell...
    return cell;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"JWScrubberLayoutToColorSegue"]) {
        if (_colorSpec) {
            id color = _trackColorsConfig[_colorSpec];
            if (color) {
                [(JWScrubberColorTableViewController *)segue.destinationViewController setSelectedColor:color];
            }
        }
        
        [(JWScrubberColorTableViewController *)segue.destinationViewController setDelegate:self];
    }
    
}


@end
