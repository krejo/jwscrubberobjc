//
//  JWScrubberLayoutTableViewController.h
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWScrubber.h"

@protocol JWScrubberLayoutDelegate;

@interface JWScrubberLayoutTableViewController : UITableViewController
@property (nonatomic) VABLayoutOptions layoutOptions;
@property (nonatomic) NSDictionary *trackColors;
@property (weak,nonatomic) id <JWScrubberLayoutDelegate> delegate;
@end

@protocol JWScrubberLayoutDelegate <NSObject>
-(void)selectionLayoutMade:(JWScrubberLayoutTableViewController*)controller;
-(void)willComplete:(JWScrubberLayoutTableViewController*)controller;
@end
