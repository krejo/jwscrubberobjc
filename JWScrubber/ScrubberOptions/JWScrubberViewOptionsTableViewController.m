//
//  JWScrubberViewOptionsTableViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/24/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "JWScrubberViewOptionsTableViewController.h"
#import "JWScrubberColorTableViewController.h"

@interface JWScrubberViewOptionsTableViewController () <JWScrubberColorDelegate>
@property (nonatomic) NSIndexPath *selected;
@property (nonatomic) NSString *colorSpec;
@property (nonatomic) NSMutableDictionary *scrubberColorsConfig;

@end

@implementation JWScrubberViewOptionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.clearsSelectionOnViewWillAppear = NO;
    
    if (_viewOptions == ScrubberViewOptionDisplayFullView) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_viewOptions == ScrubberViewOptionDisplayOnlyValueLabels) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_viewOptions == ScrubberViewOptionDisplayLabels) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    _selected = [self.tableView indexPathForSelectedRow];
    
    if (_selected) {
        [self.tableView reloadRowsAtIndexPaths:@[_selected] withRowAnimation:UITableViewRowAnimationNone];
    }

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController]) {
        NSLog(@"%s LEAVING",__func__);
        [_delegate willCompleteViewOptions:self];
    }
    
    self.clearsSelectionOnViewWillAppear = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 

- (NSDictionary*)scrubberColors {
    return [NSDictionary dictionaryWithDictionary:_scrubberColorsConfig];
}

-(void)setScrubberColors:(NSDictionary *)scrubberColors {
    if (scrubberColors) {
        _scrubberColorsConfig = [scrubberColors mutableCopy];
    } else {
        _scrubberColorsConfig = [@{} mutableCopy];
    }
}

-(void)colorSelected:(JWScrubberColorTableViewController*)controller {
    _scrubberColorsConfig[_colorSpec] = controller.selectedColor;
    _colorSpec = nil;
}


#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;


- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    // LAYOUT
    
    if (indexPath.section == 0) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if (indexPath.row == 0 && _viewOptions == ScrubberViewOptionDisplayFullView) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else if (indexPath.row == 1 && _viewOptions == ScrubberViewOptionDisplayOnlyValueLabels) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else if (indexPath.row == 2 && _viewOptions == ScrubberViewOptionDisplayLabels) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    
    // COLORS
    
    else if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            id color = _scrubberColorsConfig[JWColorBackgroundHueColor];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 1) {
            id color = _scrubberColorsConfig[JWColorBackgroundTrackGradientColor1];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 2) {
            id color = _scrubberColorsConfig[JWColorBackgroundTrackGradientColor2];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 3) {
            id color = _scrubberColorsConfig[JWColorBackgroundTrackGradientColor3];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 4) {
            id color = _scrubberColorsConfig[JWColorBackgroundHeaderGradientColor1];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        } else if (indexPath.row == 5) {
            id color = _scrubberColorsConfig[JWColorBackgroundHeaderGradientColor2];
            if (color) {
                cell.textLabel.backgroundColor = color;
            }
        }
    }
}

//ScrubberViewOptionNone     =1,
//2ScrubberViewOptionDisplayLabels,
//1ScrubberViewOptionDisplayOnlyValueLabels,
//0ScrubberViewOptionDisplayFullView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    // Section 1
    
    if (indexPath.section == 0) {
        
        _colorSpec = nil;

        if (indexPath.row == 0) {
            _viewOptions = ScrubberViewOptionDisplayFullView;
        } else if (indexPath.row == 1) {
            _viewOptions = ScrubberViewOptionDisplayOnlyValueLabels;
        } else if (indexPath.row == 2) {
            _viewOptions = ScrubberViewOptionDisplayLabels;
        }
        
        if (_selected) {
            [tableView reloadRowsAtIndexPaths:@[_selected] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        _selected = indexPath;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [_delegate selectionViewOptionsMade:self];
    }
    
    // Section 2
    // COLORS
    
    else if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            _colorSpec = (NSString*)JWColorBackgroundHueColor;
        } else if (indexPath.row == 1) {
            _colorSpec = (NSString*)JWColorBackgroundTrackGradientColor1;
        } else if (indexPath.row == 2) {
            _colorSpec = (NSString*)JWColorBackgroundTrackGradientColor2;
        } else if (indexPath.row == 3) {
            _colorSpec = (NSString*)JWColorBackgroundTrackGradientColor3;
        } else if (indexPath.row == 4) {
            _colorSpec = (NSString*)JWColorBackgroundHeaderGradientColor1;
        } else if (indexPath.row == 5) {
            _colorSpec = (NSString*)JWColorBackgroundHeaderGradientColor2;
        }
        
        [self performSegueWithIdentifier:@"JWScrubberOptionsToColorSegue" sender:self];
    }
    
}

//0 extern const NSString *JWColorBackgroundHueColor;
//extern const NSString *JWColorBackgroundHueGradientColor1;
//extern const NSString *JWColorBackgroundHueGradientColor2;
//1extern const NSString *JWColorBackgroundTrackGradientColor1;
//2extern const NSString *JWColorBackgroundTrackGradientColor2;
//3extern const NSString *JWColorBackgroundTrackGradientColor3;
//4extern const NSString *JWColorBackgroundHeaderGradientColor1;
//5extern const NSString *JWColorBackgroundHeaderGradientColor2;


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    // Configure the cell...
    return cell;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"JWScrubberOptionsToColorSegue"]) {
        if (_colorSpec) {
            id color = _scrubberColorsConfig[_colorSpec];
            if (color) {
                [(JWScrubberColorTableViewController *)segue.destinationViewController setSelectedColor:color];
            }
        }
        
        [(JWScrubberColorTableViewController *)segue.destinationViewController setDelegate:self];
    }
}


@end
