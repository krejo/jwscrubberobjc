//
//  JWScrubberSizeTableViewController.h
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWScrubber.h"

@protocol JWScrubberSizeDelegate;

@interface JWScrubberSizeTableViewController : UITableViewController
@property (nonatomic) SampleSize sampleSz;
@property (weak,nonatomic) id <JWScrubberSizeDelegate> delegate;
@end

@protocol JWScrubberSizeDelegate <NSObject>
-(void)selectionSizeMade:(JWScrubberSizeTableViewController*)controller;
@end
