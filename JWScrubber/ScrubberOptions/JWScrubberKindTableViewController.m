//
//  JWScrubberKindTableViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "JWScrubberKindTableViewController.h"

@interface JWScrubberKindTableViewController ()
@property (nonatomic) NSIndexPath *selected;
@end

//  VABOptionNone = 1,
//0 VABOptionCenter,
//  VABOptionCenterMirrored,
//1 VABOptionSingleChannelTopDown,
//2 VABOptionSingleChannelBottomUp,

@implementation JWScrubberKindTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clearsSelectionOnViewWillAppear = NO;
    
    if (_kind == VABOptionCenter) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_kind == VABOptionSingleChannelTopDown) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else if (_kind == VABOptionSingleChannelBottomUp) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]
                                    animated:NO scrollPosition:UITableViewScrollPositionNone];
    }

    _selected = [self.tableView indexPathForSelectedRow];

    if (_selected) {
        [self.tableView reloadRowsAtIndexPaths:@[_selected] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
   
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (indexPath.row == 0 && _kind == VABOptionCenter) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else if (indexPath.row == 1 && _kind == VABOptionSingleChannelTopDown) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else if (indexPath.row == 2 && _kind == VABOptionSingleChannelBottomUp) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        _kind = VABOptionCenter;
    } else if (indexPath.row == 1) {
        _kind = VABOptionSingleChannelTopDown;
    } else if (indexPath.row == 2) {
        _kind = VABOptionSingleChannelBottomUp;
    }
    
    if (_selected) {
        [tableView reloadRowsAtIndexPaths:@[_selected] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    _selected = indexPath;
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

    [_delegate selectionKindMade:self];

}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    // Configure the cell...
    return cell;
}
*/

@end
