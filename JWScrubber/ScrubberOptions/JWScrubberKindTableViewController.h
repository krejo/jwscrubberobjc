//
//  JWScrubberKindTableViewController.h
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWScrubber.h"

@protocol JWScrubberKindDelegate;

@interface JWScrubberKindTableViewController : UITableViewController
@property (nonatomic) VABKindOptions kind;
@property (weak,nonatomic) id <JWScrubberKindDelegate> delegate;
@end

@protocol JWScrubberKindDelegate <NSObject>
-(void)selectionKindMade:(JWScrubberKindTableViewController*)controller;
@end
