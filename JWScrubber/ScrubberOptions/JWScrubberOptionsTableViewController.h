//
//  JWScrubberOptionsTableViewController.h
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWScrubber.h"

@protocol JWScrubberOptionsDelegate;

@interface JWScrubberOptionsTableViewController : UITableViewController
@property (nonatomic) id optionsData;
@property (nonatomic) id scrubberColors;
@property (nonatomic) id scrubberTrackColors;
@property (nonatomic) ScrubberViewOptions viewOptions;

@property (weak,nonatomic) id <JWScrubberOptionsDelegate> delegate;
@end

@protocol JWScrubberOptionsDelegate <NSObject>
-(void)selectionMade:(JWScrubberOptionsTableViewController*)controller;
-(void)willComplete:(id)controller;

@end
