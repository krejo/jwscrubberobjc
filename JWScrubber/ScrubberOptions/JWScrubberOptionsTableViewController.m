//
//  JWScrubberOptionsTableViewController.m
//  JWScrubber
//
//  Created by JOSEPH KERR on 11/22/15.
//  Copyright © 2015 JOSEPH KERR. All rights reserved.
//

#import "JWScrubberOptionsTableViewController.h"
#import "JWScrubberSizeTableViewController.h"
#import "JWScrubberKindTableViewController.h"
#import "JWScrubberLayoutTableViewController.h"
#import "JWScrubberViewOptionsTableViewController.h"

@interface JWScrubberOptionsTableViewController ()
<
JWScrubberKindDelegate,JWScrubberLayoutDelegate,JWScrubberSizeDelegate,JWScrubberViewOptionsDelegate
>
@end

@implementation JWScrubberOptionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // self.clearsSelectionOnViewWillAppear = NO;
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Options delegate

-(void)selectionSizeMade:(JWScrubberSizeTableViewController *)controller
{
    NSLog(@"%s",__func__);
    [(NSMutableDictionary *)_optionsData setValue:@(controller.sampleSz) forKey:@"size"];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    [_delegate selectionMade:self];
}

-(void)selectionKindMade:(JWScrubberKindTableViewController *)controller
{
    NSLog(@"%s",__func__);
    [(NSMutableDictionary *)_optionsData setValue:@(controller.kind) forKey:@"kind"];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    [_delegate selectionMade:self];
}

-(void)selectionLayoutMade:(JWScrubberLayoutTableViewController *)controller
{
    NSLog(@"%s",__func__);
    [(NSMutableDictionary *)_optionsData setValue:@(controller.layoutOptions) forKey:@"layout"];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    [_delegate selectionMade:self];
}

-(void)selectionViewOptionsMade:(JWScrubberViewOptionsTableViewController *)controller
{
    NSLog(@"%s",__func__);
    _viewOptions = controller.viewOptions;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    [_delegate selectionMade:self];
}


//    NSLog(@"%s %@",__func__,[(NSMutableDictionary *)_optionsData description]);


#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UITableViewHeaderFooterView *view = [UITableViewHeaderFooterView new];
    view.contentView.backgroundColor = [UIColor lightGrayColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 22.0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    if (indexPath.row == 0){
        SampleSize ssz =  [[(NSDictionary *)_optionsData valueForKey:@"size"] unsignedIntegerValue];

        if (ssz == SampleSize4) {
            cell.detailTextLabel.text = @"SampleSize4";
        } else if (ssz == SampleSize8) {
            cell.detailTextLabel.text = @"SampleSize8";
        } else if (ssz == SampleSize10) {
            cell.detailTextLabel.text = @"SampleSize10";
        } else if (ssz == SampleSize14) {
            cell.detailTextLabel.text = @"SampleSize14";
        } else if (ssz == SampleSize18) {
            cell.detailTextLabel.text = @"SampleSize18";
        }
        
    } else if (indexPath.row == 1){
        VABKindOptions kind =  [[(NSDictionary *)_optionsData valueForKey:@"kind"] unsignedIntegerValue];

        if (kind == VABOptionCenter) {
            cell.detailTextLabel.text = @"Center";
        } else if (kind == VABOptionSingleChannelTopDown) {
            cell.detailTextLabel.text = @"TOP DOWN";
        } else if (kind == VABOptionSingleChannelBottomUp) {
            cell.detailTextLabel.text = @"BOTTOM UP";
        }
    } else if (indexPath.row == 2){
        VABLayoutOptions layout =  [[(NSDictionary *)_optionsData valueForKey:@"layout"] unsignedIntegerValue];
        NSMutableString *optionsString = [NSMutableString new];
        
        if (layout & VABLayoutOptionShowAverageSamples) {
            [optionsString appendString:@"Averages "];
        }
        if (layout & VABLayoutOptionShowHashMarks) {
            [optionsString appendString:@"Hash "];
        }
        if (layout & VABLayoutOptionStackAverages) {
            [optionsString appendString:@"Stack "];
        }
        if (layout & VABLayoutOptionOverlayAverages) {
            [optionsString appendString:@"Overlay "];
        }
        if (layout & VABLayoutOptionShowCenterLine) {
            [optionsString appendString:@"CenterLine "];
        }
        
        cell.detailTextLabel.text = optionsString;
        
    } else if (indexPath.row == 3){
        
        if (_viewOptions == ScrubberViewOptionDisplayFullView) {
            cell.detailTextLabel.text = @"FullView";
        } else if (_viewOptions == ScrubberViewOptionDisplayLabels) {
            cell.detailTextLabel.text = @"LabelsAndValues";
        } else if (_viewOptions == ScrubberViewOptionDisplayOnlyValueLabels) {
            cell.detailTextLabel.text = @"OnlyValues";
        } else {
            cell.detailTextLabel.text = @"Not Set";
        }
    }

}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    // Configure the cell...
    return cell;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"JWOptionsToSizeSegue"]) {
        SampleSize ssz =  [[(NSDictionary *)_optionsData valueForKey:@"size"] unsignedIntegerValue];
        [(JWScrubberSizeTableViewController *)segue.destinationViewController setSampleSz:ssz];
        [(JWScrubberSizeTableViewController *)segue.destinationViewController setDelegate:self];
    } else if ([segue.identifier isEqualToString:@"JWOptionsToKindSegue"]) {
        VABKindOptions kind =  [[(NSDictionary *)_optionsData valueForKey:@"kind"] unsignedIntegerValue];
        [(JWScrubberKindTableViewController *)segue.destinationViewController setKind:kind];
        [(JWScrubberKindTableViewController *)segue.destinationViewController setDelegate:self];
    } else if ([segue.identifier isEqualToString:@"JWOptionsToLayoutSegue"]) {
        VABLayoutOptions layout =  [[(NSDictionary *)_optionsData valueForKey:@"layout"] unsignedIntegerValue];
        [(JWScrubberLayoutTableViewController *)segue.destinationViewController setTrackColors:_scrubberTrackColors];
        [(JWScrubberLayoutTableViewController *)segue.destinationViewController setLayoutOptions:layout];
        [(JWScrubberLayoutTableViewController *)segue.destinationViewController setDelegate:self];
    } else if ([segue.identifier isEqualToString:@"JWOptionsToScrubberViewSegue"]) {
        [(JWScrubberViewOptionsTableViewController *)segue.destinationViewController setScrubberColors:_scrubberColors];
        [(JWScrubberViewOptionsTableViewController *)segue.destinationViewController setViewOptions:_viewOptions];
        [(JWScrubberViewOptionsTableViewController *)segue.destinationViewController setDelegate:self];
    }

}

- (IBAction)done:(id)sender {
    [_delegate willComplete:self];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

//    [_delegate selectionMade:self];
//    [self dismissViewControllerAnimated:YES completion:nil];


-(void)willComplete:(JWScrubberLayoutTableViewController*)controller {
    _scrubberTrackColors = controller.trackColors;
    [_delegate selectionMade:self];
}

-(void)willCompleteViewOptions:(JWScrubberViewOptionsTableViewController*)controller {
    _scrubberColors = controller.scrubberColors;
    [_delegate selectionMade:self];
}


@end
